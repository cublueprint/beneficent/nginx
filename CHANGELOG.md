# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [6.20.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.20.3...v6.20.4) (2022-03-21)

### [6.20.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.20.2...v6.20.3) (2022-03-20)

### [6.20.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.20.1...v6.20.2) (2022-03-19)

### [6.20.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.20.0...v6.20.1) (2022-03-18)

## [6.20.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.19.1...v6.20.0) (2022-03-17)


### Features

* publish client v7.4.5 ([54e3f94](https://gitlab.com/cublueprint/beneficent/nginx/commit/54e3f94a46b76c98e20a584106788eddf696160d))

### [6.19.1](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/nginx/compare/v6.19.0...v6.19.1) (2022-03-16)

## [6.19.0](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/nginx/compare/v6.18.3...v6.19.0) (2022-03-15)


### Features

* publish client v7.4.4 ([940b0dd](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/nginx/commit/940b0dd0d23cc07716248e30480ce7e7bc9c5ab7))

### [6.18.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.18.2...v6.18.3) (2022-03-15)

### [6.18.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.18.1...v6.18.2) (2022-03-14)

### [6.18.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.18.0...v6.18.1) (2022-03-13)

## [6.18.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.17.0...v6.18.0) (2022-03-13)


### Features

* publish client v7.3.7 ([fd0bd07](https://gitlab.com/cublueprint/beneficent/nginx/commit/fd0bd074225a8304e851f77682055eb03a7566c5))
* publish client v7.4.0 ([a69b65d](https://gitlab.com/cublueprint/beneficent/nginx/commit/a69b65d728f03a7e0aac6abe0efedc8e8b031429))

## [6.17.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.16.1...v6.17.0) (2022-03-12)


### Features

* publish client v7.3.6 ([e94c4e5](https://gitlab.com/cublueprint/beneficent/nginx/commit/e94c4e51211d6ad6611e549276c9b30da17a15b0))

### [6.16.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.16.0...v6.16.1) (2022-03-11)

## [6.16.0](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/nginx/compare/v6.15.3...v6.16.0) (2022-03-10)


### Features

* publish client v7.3.4 ([c81b0d1](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/nginx/commit/c81b0d15993e21ce95ff3ac6500a08489ae357ab))
* publish client v7.3.4 ([92910cc](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/nginx/commit/92910cc8b7090eaf4285124fea98331451c35364))

### [6.15.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.15.2...v6.15.3) (2022-03-10)

### [6.15.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.15.1...v6.15.2) (2022-03-09)

### [6.15.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.15.0...v6.15.1) (2022-03-08)

## [6.15.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.14.0...v6.15.0) (2022-03-07)


### Features

* publish client v7.3.1 ([c4941fe](https://gitlab.com/cublueprint/beneficent/nginx/commit/c4941fed5a869a3b8a869baadabfa8f98fab2854))

## [6.14.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.13.4...v6.14.0) (2022-03-07)


### Features

* publish client v7.3.0 ([0d0ef94](https://gitlab.com/cublueprint/beneficent/nginx/commit/0d0ef949a82c7745ddb14254bdb09a37ec0153e1))

### [6.13.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.13.3...v6.13.4) (2022-03-06)

### [6.13.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.13.2...v6.13.3) (2022-03-05)

### [6.13.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.13.1...v6.13.2) (2022-03-04)

### [6.13.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.13.0...v6.13.1) (2022-03-03)

## [6.13.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.12.2...v6.13.0) (2022-03-02)


### Features

* publish client v7.2.0 ([27355c3](https://gitlab.com/cublueprint/beneficent/nginx/commit/27355c3a79cca2e63178ae2b7921e6cbfcdcdd3f))

### [6.12.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.12.1...v6.12.2) (2022-03-01)

### [6.12.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.12.0...v6.12.1) (2022-02-28)

## [6.12.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.11.2...v6.12.0) (2022-02-27)


### Features

* publish client v7.1.0 ([d7726e9](https://gitlab.com/cublueprint/beneficent/nginx/commit/d7726e987aa1b9ef37ae7fcfd7d7c4974448853e))

### [6.11.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.11.1...v6.11.2) (2022-02-26)

### [6.11.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.11.0...v6.11.1) (2022-02-25)

## [6.11.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.10.5...v6.11.0) (2022-02-24)


### Features

* publish client v7.0.0 ([b572d1d](https://gitlab.com/cublueprint/beneficent/nginx/commit/b572d1dac5e0f52c198c3204b8c59255fd408f2e))

### [6.10.5](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.10.4...v6.10.5) (2022-02-24)

### [6.10.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.10.3...v6.10.4) (2022-02-23)

### [6.10.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.10.2...v6.10.3) (2022-02-22)

### [6.10.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.10.1...v6.10.2) (2022-02-21)

### [6.10.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.10.0...v6.10.1) (2022-02-20)

## [6.10.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.9.3...v6.10.0) (2022-02-19)


### Features

* publish client v6.2.10 ([6d15ef4](https://gitlab.com/cublueprint/beneficent/nginx/commit/6d15ef45741c3ce64ed66e9c3d580ae5afe08829))

### [6.9.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.9.2...v6.9.3) (2022-02-18)

### [6.9.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.9.1...v6.9.2) (2022-02-17)

### [6.9.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.9.0...v6.9.1) (2022-02-16)

## [6.9.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.8.0...v6.9.0) (2022-02-15)


### Features

* publish client v6.2.7 ([7c3a429](https://gitlab.com/cublueprint/beneficent/nginx/commit/7c3a4299e989dd1b6e25a62189ed15ca652e086a))

## [6.8.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.7.2...v6.8.0) (2022-02-15)


### Features

* publish client v6.2.6 ([f211fc2](https://gitlab.com/cublueprint/beneficent/nginx/commit/f211fc2e8a84c09b78b246fad80f396d62b219e1))

### [6.7.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.7.1...v6.7.2) (2022-02-15)

### [6.7.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.7.0...v6.7.1) (2022-02-14)

## [6.7.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.6.2...v6.7.0) (2022-02-13)


### Features

* publish client v6.2.3 ([1b2c881](https://gitlab.com/cublueprint/beneficent/nginx/commit/1b2c88159bb422a5d3d3f8303de88e621e91e703))

### [6.6.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.6.1...v6.6.2) (2022-02-13)

### [6.6.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.6.0...v6.6.1) (2022-02-12)

## [6.6.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.5.2...v6.6.0) (2022-02-12)


### Features

* publish client v6.2.2 ([7f5436c](https://gitlab.com/cublueprint/beneficent/nginx/commit/7f5436c4bb98b3094e4dd67e2b33ea0e7358cdc7))

### [6.5.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.5.1...v6.5.2) (2022-02-12)

### [6.5.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.5.0...v6.5.1) (2022-02-11)

## [6.5.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.4.6...v6.5.0) (2022-02-11)


### Features

* publish client v6.2.1 ([c659c84](https://gitlab.com/cublueprint/beneficent/nginx/commit/c659c840a4da689ef26a2b503405e3ceb604212e))

### [6.4.6](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.4.5...v6.4.6) (2022-02-10)

### [6.4.5](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.4.4...v6.4.5) (2022-02-09)

### [6.4.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.4.3...v6.4.4) (2022-02-08)

### [6.4.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.4.2...v6.4.3) (2022-02-07)

### [6.4.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.4.1...v6.4.2) (2022-02-06)

### [6.4.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.4.0...v6.4.1) (2022-02-05)

## [6.4.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.3.5...v6.4.0) (2022-02-04)


### Features

* publish client v6.2.0 ([a7fe812](https://gitlab.com/cublueprint/beneficent/nginx/commit/a7fe812d159d45354e98aec69b6e050acf89432f))

### [6.3.5](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.3.4...v6.3.5) (2022-02-03)

### [6.3.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.3.3...v6.3.4) (2022-02-02)

### [6.3.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.3.2...v6.3.3) (2022-02-01)

### [6.3.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.3.1...v6.3.2) (2022-01-31)

### [6.3.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.3.0...v6.3.1) (2022-01-30)

## [6.3.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.2.3...v6.3.0) (2022-01-29)


### Features

* publish client v6.1.0 ([8399134](https://gitlab.com/cublueprint/beneficent/nginx/commit/83991340ea73925d0cb11345a90c0155a301cb4a))

### [6.2.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.2.2...v6.2.3) (2022-01-28)

### [6.2.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.2.1...v6.2.2) (2022-01-27)

### [6.2.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.2.0...v6.2.1) (2022-01-26)

## [6.2.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.11...v6.2.0) (2022-01-25)


### Features

* publish client v6.0.14 ([a9335ae](https://gitlab.com/cublueprint/beneficent/nginx/commit/a9335ae822163427543853d92f0811d8d6c550bd))

### [6.1.11](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.10...v6.1.11) (2022-01-24)

### [6.1.10](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.9...v6.1.10) (2022-01-23)

### [6.1.9](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.8...v6.1.9) (2022-01-22)

### [6.1.8](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.7...v6.1.8) (2022-01-21)

### [6.1.7](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.6...v6.1.7) (2022-01-20)

### [6.1.6](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.5...v6.1.6) (2022-01-19)

### [6.1.5](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.4...v6.1.5) (2022-01-18)

### [6.1.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.3...v6.1.4) (2022-01-17)

### [6.1.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.2...v6.1.3) (2022-01-16)

### [6.1.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.1...v6.1.2) (2022-01-15)

### [6.1.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.1.0...v6.1.1) (2022-01-14)

## [6.1.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.0.6...v6.1.0) (2022-01-13)


### Features

* publish client v6.0.1 ([8a92797](https://gitlab.com/cublueprint/beneficent/nginx/commit/8a927971f1e6c729b20ba09fc7850a8ecefa1761))

### [6.0.6](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.0.5...v6.0.6) (2022-01-12)

### [6.0.5](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.0.4...v6.0.5) (2022-01-11)

### [6.0.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.0.3...v6.0.4) (2022-01-11)


### Bug Fixes

* change sed delimeter ([862c299](https://gitlab.com/cublueprint/beneficent/nginx/commit/862c299b2c15edde2639495f169b0d18d0d1cdd5))

### [6.0.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.0.2...v6.0.3) (2022-01-11)

### [6.0.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.0.1...v6.0.2) (2022-01-11)


### Bug Fixes

* remove e flag from sed command ([6804be2](https://gitlab.com/cublueprint/beneficent/nginx/commit/6804be218af20132ed4fe838e0bc0331c06a8d13))

### [6.0.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v6.0.0...v6.0.1) (2022-01-11)


### Bug Fixes

* expose API_URL variable ([ec293c6](https://gitlab.com/cublueprint/beneficent/nginx/commit/ec293c6ece43b4106759b7171dafa660282e9693))

## [6.0.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v5.1.1...v6.0.0) (2022-01-11)


### ⚠ BREAKING CHANGES

* remove build config script and use sed

### Features

* remove build config script and use sed ([2c8138e](https://gitlab.com/cublueprint/beneficent/nginx/commit/2c8138e16fa574fa0b838f5e72d96ba917e40969))

### [5.1.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v5.1.0...v5.1.1) (2022-01-11)


### Bug Fixes

* remove additonal job to build config ([8ae6754](https://gitlab.com/cublueprint/beneficent/nginx/commit/8ae675419dc3707eee10b7aa27cbf2f6fe6c6121))

## [5.1.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v5.0.5...v5.1.0) (2022-01-11)


### Features

* add job to build nginx config ([f384323](https://gitlab.com/cublueprint/beneficent/nginx/commit/f384323adf86b0c4e8723f48b53fe824c90bab76))

### [5.0.5](https://gitlab.com/cublueprint/beneficent/nginx/compare/v5.0.4...v5.0.5) (2022-01-11)


### Bug Fixes

* add openrc to start docker service ([ee4b60a](https://gitlab.com/cublueprint/beneficent/nginx/commit/ee4b60a46cb92c11a943d216448a1cb3347130ad))

### [5.0.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v5.0.3...v5.0.4) (2022-01-11)

### [5.0.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v5.0.2...v5.0.3) (2022-01-11)


### Bug Fixes

* start docker without service command ([f46625b](https://gitlab.com/cublueprint/beneficent/nginx/commit/f46625ba0e94049b65d5dfde9eac0ba7dacca851))

### [5.0.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v5.0.1...v5.0.2) (2022-01-11)


### Bug Fixes

* start docker service in script ([1dd2a82](https://gitlab.com/cublueprint/beneficent/nginx/commit/1dd2a8299d7cdf372aa823676d8958e82ab0262b))

### [5.0.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v5.0.0...v5.0.1) (2022-01-11)


### Bug Fixes

* install docker to node image ([2e4cf6f](https://gitlab.com/cublueprint/beneficent/nginx/commit/2e4cf6f078900eb5c075843c74c970de3c7e1960))

## [5.0.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v4.1.1...v5.0.0) (2022-01-11)


### ⚠ BREAKING CHANGES

* add script to build nginx config

### Features

* add script to build nginx config ([9278442](https://gitlab.com/cublueprint/beneficent/nginx/commit/92784427b6bcac2c438a7894ad2a0fd364078d85))

### [4.1.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v4.1.0...v4.1.1) (2022-01-11)


### Bug Fixes

* inject api url during build ([c285664](https://gitlab.com/cublueprint/beneficent/nginx/commit/c285664698875602935bfa405894b8b3a2dd1b15))

## [4.1.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v4.0.2...v4.1.0) (2022-01-11)


### Features

* expose api url from heroku ([09cf049](https://gitlab.com/cublueprint/beneficent/nginx/commit/09cf049c5b53367a5793c8ae55cddd1bfc196e48))

### [4.0.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v4.0.1...v4.0.2) (2022-01-11)

### [4.0.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v4.0.0...v4.0.1) (2022-01-11)


### Bug Fixes

* improve deploy guard ([20d56a6](https://gitlab.com/cublueprint/beneficent/nginx/commit/20d56a6f7203fd7520e65fec8438ccd0789d8d66))
* split script into individual commands ([b05cb2c](https://gitlab.com/cublueprint/beneficent/nginx/commit/b05cb2c45f6fd410425888213ec856680c1a5312))
* update hierarchy of rules ([ad39ad1](https://gitlab.com/cublueprint/beneficent/nginx/commit/ad39ad148faebb50faebc8875d49bd74f7a10665))
* use rules to control deploy ([0774bb5](https://gitlab.com/cublueprint/beneficent/nginx/commit/0774bb51d6a85cb98d5f78084e8d99a5c23d39ee))

## [4.0.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v3.0.0...v4.0.0) (2022-01-11)


### ⚠ BREAKING CHANGES

* add guard to check for deploy during schedules

### Features

* add guard to check for deploy during schedules ([e524f66](https://gitlab.com/cublueprint/beneficent/nginx/commit/e524f66369c3f57a9c434a07209f2bff6bfb35ae))

## [3.0.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v2.0.0...v3.0.0) (2022-01-11)


### ⚠ BREAKING CHANGES

* move release pipeline into a template

### Features

* move release pipeline into a template ([21e4af5](https://gitlab.com/cublueprint/beneficent/nginx/commit/21e4af5555e28108468fac6033d6d16dc6d5b311))

## [2.0.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.9...v2.0.0) (2022-01-11)


### ⚠ BREAKING CHANGES

* inject proxy url from the heroku envoirnment

### Features

* inject proxy url from the heroku envoirnment ([050cb49](https://gitlab.com/cublueprint/beneficent/nginx/commit/050cb49f73c5009ec7ad60d7db4bc13a5fba476a))

### [1.13.9](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.8...v1.13.9) (2022-01-11)

### [1.13.8](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.7...v1.13.8) (2022-01-10)

### [1.13.7](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.6...v1.13.7) (2022-01-10)

### [1.13.6](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.5...v1.13.6) (2022-01-10)

### [1.13.5](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.4...v1.13.5) (2022-01-10)

### [1.13.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.3...v1.13.4) (2022-01-10)

### [1.13.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.2...v1.13.3) (2022-01-10)

### [1.13.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.1...v1.13.2) (2022-01-10)

### [1.13.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.13.0...v1.13.1) (2022-01-10)

## [1.13.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.12.0...v1.13.0) (2022-01-09)


### Features

* publish client v5.1.1 ([392a3b5](https://gitlab.com/cublueprint/beneficent/nginx/commit/392a3b5f7604193724817a93d1fff16789785cb3))

## [1.12.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.11.0...v1.12.0) (2022-01-09)


### Features

* publish client v5.1.0 ([a1f7f22](https://gitlab.com/cublueprint/beneficent/nginx/commit/a1f7f22a1f4312c5d961d4a331e8aa8f97723ef6))

## [1.11.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.10.5...v1.11.0) (2022-01-09)


### Features

* publish client v5.0.11 ([0d56f2b](https://gitlab.com/cublueprint/beneficent/nginx/commit/0d56f2b340caaf1f2af053aea7cccbdf60f25d48))

### [1.10.5](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.10.4...v1.10.5) (2022-01-09)

### [1.10.4](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.10.3...v1.10.4) (2022-01-06)

### [1.10.3](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.10.2...v1.10.3) (2022-01-06)

### [1.10.2](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.10.1...v1.10.2) (2022-01-05)

### [1.10.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.10.0...v1.10.1) (2022-01-02)

## [1.10.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.9.0...v1.10.0) (2021-12-30)


### Features

* publish client v5.0.5 ([f313362](https://gitlab.com/cublueprint/beneficent/nginx/commit/f31336252347872cf9ddcaf90829e0549bed1872))

## [1.9.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.8.0...v1.9.0) (2021-12-29)


### Features

* publish client v5.0.4 ([1b1c39e](https://gitlab.com/cublueprint/beneficent/nginx/commit/1b1c39ec2e53b14e56b1003c986f1f79b34a168e))

## [1.8.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.7.0...v1.8.0) (2021-12-23)


### Features

* publish client v5.0.2 ([0fd9012](https://gitlab.com/cublueprint/beneficent/nginx/commit/0fd901241764b117337cf97cb8f22395bd4076ca))

## [1.7.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.6.0...v1.7.0) (2021-12-15)


### Features

* publish client v5.0.1 ([5b74e02](https://gitlab.com/cublueprint/beneficent/nginx/commit/5b74e028a318e385643f4b710aa372f0fcc1a72c))

## [1.6.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.5.1...v1.6.0) (2021-12-13)


### Features

* publish client v5.0.0 ([7a1d80f](https://gitlab.com/cublueprint/beneficent/nginx/commit/7a1d80f10b6b3145efbfc41686e9cb29a6e5263d))

### [1.5.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.5.0...v1.5.1) (2021-12-12)

## [1.5.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.4.0...v1.5.0) (2021-12-03)


### Features

* publish client v4.0.0 ([41795ec](https://gitlab.com/cublueprint/beneficent/nginx/commit/41795ec4691676eaee000d94377ead0f2dd77f0f))

## [1.4.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.3.0...v1.4.0) (2021-11-24)


### Features

* publish client v3.1.1 ([4f5bc91](https://gitlab.com/cublueprint/beneficent/nginx/commit/4f5bc915c6862dee761fa7382920168ee4f25580))

## [1.3.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.0.0...v1.3.0) (2021-11-24)


### Features

* add release pipeline (EN-362) ([687a7fc](https://gitlab.com/cublueprint/beneficent/nginx/commit/687a7fc12bdfd254f863ace7ee4f8c9bbffdb47b))
* publish client main ([1cb71df](https://gitlab.com/cublueprint/beneficent/nginx/commit/1cb71df74aec4691f66aec58bb5a2e0176a296ff))
* publish client main ([a94b16c](https://gitlab.com/cublueprint/beneficent/nginx/commit/a94b16c62483a498f565fa3d7553d25623aa1ddd))
* publish client main ([6210fc6](https://gitlab.com/cublueprint/beneficent/nginx/commit/6210fc61256aa47e98c57b468122245905d30735))
* publish client main ([8130a91](https://gitlab.com/cublueprint/beneficent/nginx/commit/8130a9188280540a542fcf96e49ca2cbf644000e))

### [1.2.1](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.2.0...v1.2.1) (2021-09-12)

## [1.2.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.1.0...v1.2.0) (2021-09-12)


### Features

* publish client main ([6210fc6](https://gitlab.com/cublueprint/beneficent/nginx/commit/6210fc61256aa47e98c57b468122245905d30735))

## [1.1.0](https://gitlab.com/cublueprint/beneficent/nginx/compare/v1.0.0...v1.1.0) (2021-09-12)


### Features

* add release pipeline (EN-362) ([687a7fc](https://gitlab.com/cublueprint/beneficent/nginx/commit/687a7fc12bdfd254f863ace7ee4f8c9bbffdb47b))
* publish client main ([8130a91](https://gitlab.com/cublueprint/beneficent/nginx/commit/8130a9188280540a542fcf96e49ca2cbf644000e))

## 1.0.0 (2021-08-11)
